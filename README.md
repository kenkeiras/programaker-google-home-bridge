# PrograMaker <--> Google Actions bridge

This is a work in progress to program Google Actions, including Google Home devices, through [PrograMaker](https://programaker.com).

This is not completed, consider it as an exploratory work in progress. It requires some additional configuration on Google Actions to make it work.
If you're interested on using it before it's completed, get in contact to get the configuration details.

## Features

 - [X] Basic question/reply
 - [ ] Authentication (this is needed to be opened to the public)
 - [ ] Sending notifications without prompting
 - [ ] Prepare for release (this includes generating test documentation)
