import json
import logging
import os
import threading
import traceback

from flask import Flask, request
from programaker_bridge import (BlockArgument, BlockContext, ProgramakerBridge,
                                VariableBlockArgument)

app = Flask(__name__)


bridge = ProgramakerBridge(
    name="Google Home",
    is_public=False,
    events=["on_enter", "on_message"],
    collections=[],
    endpoint=(os.getenv("ENDPOINT", None) or open(".endpoint.txt").read().strip()),
    token=(os.getenv("TOKEN", None) or open(".token.txt").read().strip()),
    allow_multiple_connections=False,
)

on_enter_event = bridge.events.on_enter
on_enter_event.add_trigger_block(
    id="on_enter",
    message="When the conversation starts. Set initial message: %1",
    arguments=[VariableBlockArgument()],
    save_to=BlockContext.ARGUMENTS[0],
    # requires_downstream=("reply", "reply_and_stop"),
)

on_message_event = bridge.events.on_message
on_message_event.add_trigger_block(
    id="on_message",
    message="On message. Set %1",
    arguments=[VariableBlockArgument()],
    save_to=BlockContext.ARGUMENTS[0],
    # requires_downstream=("reply", "reply_and_stop"),
)

QUERIES = {}
RESPONSES = {}
TIMEOUT = 30  # Seconds
DEFAULT_RESPONSE = "oops, no response in time"


@bridge.operation(
    id="reply",  # Give it an ID
    message="Reply: %1",  # Set block message
    arguments=[
        BlockArgument(str, "Hi!"),
    ],
    # requires_upstream=(on_enter_event, on_message_event),
)
def reply(message, extra_data):
    global QUERIES
    global RESPONSES

    session_id = extra_data.last_monitor_value["session"]["id"]
    logging.info('Replying "{}" to session "{}"'.format(message, session_id))

    RESPONSES[session_id] = {"text": message, "close": False}
    QUERIES[session_id].set()
    return True


@bridge.operation(
    id="reply_and_stop",  # Give it an ID
    message="Reply: %1 and finish convesation",  # Set block message
    arguments=[BlockArgument(str, "Bye!"),],
    # requires_upstream=(on_enter_event, on_message_event),
)
def reply_and_stop(message, extra_data):
    global QUERIES
    global RESPONSES

    session_id = extra_data.last_monitor_value["session"]["id"]
    logging.info(
        'Replying "{}" to session "{}" and closing'.format(message, session_id)
    )

    RESPONSES[session_id] = {"text": message, "close": True}
    QUERIES[session_id].set()
    return True


## HTTP API, to communicate with Google Actions
@app.route("/google-home", methods=["POST"])
def google_home():
    global QUERIES
    global RESPONSES

    logging.info(json.dumps(request.json, indent=4))
    if request.json["handler"]["name"] == "enter":
        on_enter_event.send(
            to_user=None, content=request.json["intent"]["query"], event=request.json,
        )
    else:
        on_message_event.send(
            to_user=None, content=request.json["intent"]["query"], event=request.json,
        )

    event = QUERIES[request.json["session"]["id"]] = threading.Event()

    if event.wait(TIMEOUT):
        response = RESPONSES[request.json["session"]["id"]]
        del RESPONSES[request.json["session"]["id"]]
    else:
        response = {"text": DEFAULT_RESPONSE, "close": True}

    del QUERIES[request.json["session"]["id"]]

    result = {}
    result["prompt"] = {
        "firstSimple": {"speech": response["text"], "text": response["text"],}
    }

    if response["close"]:
        result["scene"] = {
            "name": request.json["scene"]["name"],
            "slotFillingStatus": "FINAL",
            "slots": {},
            "next": {"name": "actions.scene.END_CONVERSATION",},
        }

    return result


if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO, format="%(levelname)-8s %(message)s")

    logging.info("Starting GoogleHome bridge")

    threading.Thread(
        target=lambda: app.run(
            host=os.getenv("FLASK_HOST", "127.0.0.1"),
            port=int(os.getenv("FLASK_PORT", 1234)),
        )
    ).start()

    try:
        bridge.run()
    except:
        traceback.print_exc()
        pass
    os._exit(1)
