FROM python:3-alpine

WORKDIR /usr/src/app

# Note that everything is uninstalled later.
ADD requirements.txt ./

RUN pip install -U -r requirements.txt

COPY main.py ./

EXPOSE 1234

CMD ["python", "main.py"]
